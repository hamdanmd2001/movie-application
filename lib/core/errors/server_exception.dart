import 'package:movie_application/core/network/error_model.dart';

class ServerException implements Exception {
  final ErrorModel errorModel;

  const ServerException(this.errorModel);
}
