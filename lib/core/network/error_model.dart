import 'package:equatable/equatable.dart';

class ErrorModel extends Equatable {
  final int statusCode;
  final String statusMessage;
  final bool success;

  const ErrorModel({
    required this.statusCode,
    required this.statusMessage,
    required this.success,
  });

  factory ErrorModel.fromJson(Map<String, dynamic> jsonResponse) {
    return ErrorModel(
      statusCode: jsonResponse['status_code'],
      statusMessage: jsonResponse['status_message'],
      success: jsonResponse['success'],
    );
  }

  @override
  List<Object?> get props => [
        statusCode,
        statusMessage,
        success,
      ];
}
