class ApiConstants {
  static const String baseUrl = 'https://api.themoviedb.org/3';
  static const String apiKey = 'api_key=3f3f7cf8c2fa4316a7290aba3c396f30';

  static const nowPlayingMoviePath = '$baseUrl/movie/now_playing?$apiKey';
  static const popularMoviePath = '$baseUrl/movie/top_rated?$apiKey';
  static const topRatedMoviePath = '$baseUrl/movie/popular?$apiKey';
}
