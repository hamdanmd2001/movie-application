import 'package:dartz/dartz.dart';
import 'package:movie_application/movies/domain/entities/movie.dart';
import 'package:movie_application/movies/domain/repository/base_movie_repository.dart';

import '../../../core/errors/failure.dart';

class GetNowPlayingMovies {
  final BaseMovieRepository baseMovieRepository;

  GetNowPlayingMovies(this.baseMovieRepository);

  Future<Either<Failure, List<Movie>>>execute() async =>
      await baseMovieRepository.getNowPlayingMovies();
}
