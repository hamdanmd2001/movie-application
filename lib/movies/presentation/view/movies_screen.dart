import 'package:flutter/material.dart';
import 'package:movie_application/movies/data/data_source/remote/base_movie_data_source.dart';
import 'package:movie_application/movies/data/data_source/remote/movie_data_source.dart';
import 'package:movie_application/movies/data/repository/movie_repository.dart';
import 'package:movie_application/movies/domain/repository/base_movie_repository.dart';
import 'package:movie_application/movies/domain/use_cases/get_now_playing_movies_use_case.dart';
class MovieScreen extends StatefulWidget {
  const MovieScreen({Key? key}) : super(key: key);

  @override
  State<MovieScreen> createState() => _MovieScreenState();
}

class _MovieScreenState extends State<MovieScreen> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold();
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  void _getData() async {
    BaseMovieDataSource baseMovieDataSource = MovieDataSource();
    BaseMovieRepository baseMovieRepository = MovieRepository(baseMovieDataSource);
    final result = await GetNowPlayingMoviesUseCase(baseMovieRepository).execute();
    print(result);
  }
}
