import 'package:dartz/dartz.dart';
import 'package:movie_application/core/errors/failure.dart';
import 'package:movie_application/core/errors/server_exception.dart';
import 'package:movie_application/movies/data/data_source/remote/base_movie_data_source.dart';
import 'package:movie_application/movies/domain/entities/movie.dart';
import 'package:movie_application/movies/domain/repository/base_movie_repository.dart';

class MovieRepository extends BaseMovieRepository {
  final BaseMovieDataSource baseMovieDataSource;

  MovieRepository(this.baseMovieDataSource);

  @override
  Future<Either<Failure, List<Movie>>> getNowPlayingMovies() async {
    final result = await baseMovieDataSource.getNowPlayingMovies();
    try {
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorModel.statusMessage));
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getPopularMovies() async {
    final result = await baseMovieDataSource.getPopularMovies();
    try {
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorModel.statusMessage));
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getTopRatedMovies() async {
    final result = await baseMovieDataSource.getTopRatedMovies();
    try {
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorModel.statusMessage));
    }
  }
}
