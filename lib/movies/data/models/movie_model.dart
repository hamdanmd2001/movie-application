import 'package:movie_application/movies/domain/entities/movie.dart';

class MovieModel extends Movie {
  const MovieModel({
    required super.id,
    required super.backdropPath,
    required super.title,
    required super.overview,
    required super.voteAverage,
    required super.releaseDate,
    required super.genreIds,
  });
  factory MovieModel.fromJson(Map<String, dynamic> jsonResponse) => MovieModel(
        id: jsonResponse['id'],
        backdropPath: jsonResponse['backdrop_path'],
        title: jsonResponse['title'],
        overview: jsonResponse['overview'],
        voteAverage: jsonResponse['vote_average'].toDouble(),
        releaseDate: jsonResponse['release_date'],
        genreIds: List<int>.from(jsonResponse['genre_ids'].map((e) => e)),
      );
}
