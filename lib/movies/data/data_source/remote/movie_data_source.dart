import 'package:dio/dio.dart';
import 'package:movie_application/core/errors/server_exception.dart';
import 'package:movie_application/movies/data/data_source/remote/base_movie_data_source.dart';
import 'package:movie_application/movies/data/models/movie_model.dart';
import 'package:movie_application/movies/domain/entities/movie.dart';
import 'package:movie_application/movies/domain/repository/base_movie_repository.dart';
import '../../../../core/network/api_constants.dart';
import '../../../../core/network/error_model.dart';

class MovieDataSource extends BaseMovieDataSource {
  @override
  Future<List<MovieModel>> getNowPlayingMovies() async {
    final response = await Dio().get(ApiConstants.nowPlayingMoviePath);

    if (response.statusCode == 200) {
      return (response.data['results'] as List)
          .map((x) => MovieModel.fromJson(x))
          .toList();
    } else {
      throw ServerException(ErrorModel.fromJson(response.data));
    }
  }

  @override
  Future<List<Movie>> getPopularMovies() async {
    final response = await Dio().get(ApiConstants.popularMoviePath);

    if (response.statusCode == 200) {
      return (response.data['results'] as List)
          .map((x) => MovieModel.fromJson(x))
          .toList();
    } else {
      throw ServerException(ErrorModel.fromJson(response.data));
    }
  }

  @override
  Future<List<Movie>> getTopRatedMovies() async {
    final response = await Dio().get(ApiConstants.topRatedMoviePath);

    if (response.statusCode == 200) {
      return (response.data['results'] as List)
          .map((x) => MovieModel.fromJson(x))
          .toList();
    } else {
      throw ServerException(ErrorModel.fromJson(response.data));
    }
  }
}
