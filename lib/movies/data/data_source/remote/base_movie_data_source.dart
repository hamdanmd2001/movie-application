import '../../../domain/entities/movie.dart';

abstract class BaseMovieDataSource{
  Future<List<Movie>> getNowPlayingMovies();

  Future<List<Movie>> getPopularMovies();

  Future<List<Movie>> getTopRatedMovies();
}